/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pilasycolas;

/**
 *
 * @author MAtias Herrera C
 */
public class Pila<T>{
    Nodo<T> pila;

    public Pila(){
        this.pila = null;
    }
     public void apilar(T dato){
         Nodo<T> aux;
         aux= new Nodo(dato,pila);
         pila=aux;
     }
     
     public T desapilar(){
         T resultado;
         if(pila==null)
             System.out.println("pila vacia");
         resultado=pila.dato;
         pila=pila.siguiente;
         return resultado;
     }
     
     public void eliminar(Pila<T> uno,Pila<T> dos){
     Pila<T> aux=null;
     
     while(dos!=null){
        
     if(uno.equals(dos)){
      uno.desapilar();
     }else{
     aux.apilar(uno.desapilar());
     }
     if(uno==null){
     dos.desapilar();
     while(aux!=null){
     uno.apilar(aux.desapilar());
     }
     
     }
     
     
     }
     
     
     }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pila p=new Pila();
        Pila P2=new Pila();
        p.apilar(1);
        p.apilar(2);
        p.apilar(3);
        P2.apilar(4);
        P2.apilar(2);
        
        p.eliminar(p, P2);
        while(p!=null){
        
            System.out.println( p.desapilar());
        
        }
    }
    
}
